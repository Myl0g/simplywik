#! /usr/bin/env python3.7

from colorama import Fore
import click
import yaml
import time
import os
import shutil
from simplywik import config, md
from typing import Callable


@click.group()
def cli():
    pass


@cli.command()
def init():
    """Initializes a new simplywik Wiki in the current directory."""

    if os.path.isfile("./simplywik.yml"):
        with open("./simplywik.yml", "r") as conf_file:
            conf = yaml.safe_load(conf_file)
            config.verify_config(conf)
            print(
                Fore.YELLOW + "Valid configuration already present! Did you mean to do something else?")
    else:
        with open("./simplywik.yml", "w") as conf_file:
            conf_file.write(yaml.safe_dump(config.create_config()))
            print(Fore.GREEN + "A new config file named \"simplywik.yml\" has been created in your current directory. Edit as you please!")


@cli.command()
def build():
    """Build the Wiki according to the config file."""

    start = time.time()

    if not os.path.isfile("./simplywik.yml"):
        print(Fore.RED + "No config file found in the current directory! Run \"simplywik init\" to generate one.")
        exit(2)

    conf = yaml.safe_load(open("./simplywik.yml", "r"))
    config.verify_config(conf)

    if conf['clear_output_dir_on_build'] and os.path.exists(conf['output_directory']):
        shutil.rmtree(conf['output_directory'])

    if not os.path.exists(conf['output_directory']):
        os.mkdir(conf['output_directory'])

    if not os.path.exists("{}/index.md".format(conf['data_directory'])):
        print(Fore.YELLOW + "Warning: No \"index.md\" file found in the data directory! Browsers will look for the index file first, so it is recommended to create a welcome page with that file name.")

    def buildHelper(path: str) -> None:
        for root, subdirs, files in os.walk(path):
            for file in files:
                input_path = "{}/{}".format(root, file)
                output_path = "{}/{}".format(root.replace(
                    "./data", conf['output_directory']), str(file).replace(".md", ".html"))
                md.buildMarkdown(input_path, output_path)

            for subdir in subdirs:
                buildHelper(subdir)

    buildHelper(conf['data_directory'])

    end = time.time()
    print(Fore.GREEN + "Built Wiki successfully in {} seconds.".format(round(end - start, 3)))


if __name__ == "__main__":
    cli()
